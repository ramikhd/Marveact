import {
    StyleSheet,
    StatusBar,
    SafeAreaView,
    useColorScheme
} from 'react-native';


import React, { useState, useEffect } from 'react';

import NavigatorBottom from "./CompenentSmall/NavigatorBottom";
import store from "./Redux/Store";
import {Provider} from "react-redux";
import {getFavoritesList, getFilmList, getSeriesList, switchMode} from "./Redux/Actions/ActionContent";
import {ActivityIndicator, MD2Colors, Provider as PaperProvider} from 'react-native-paper';
export default function App() {


    const [loading, setLoading] = useState(true);
    const mode=useColorScheme()
     useEffect(() => {
        const loadFilms = async () => {
            await (getFilmList())(store.dispatch)

        };
        const loadSeries = async () => {
            await (getSeriesList())(store.dispatch)
        };

        const loadFavorites = async () => {
            await (getFavoritesList())(store.dispatch)
            setLoading(false);
            store.dispatch(switchMode(mode))
        };
         //const dispatch = useDispatch();

        loadFilms();
        loadSeries();
        loadFavorites();
        //dispatch(switchMode(mode));

    },[]);


    const colorScheme = useColorScheme();

    const handleColorSchemeChange = (newColorScheme) => {
        store.dispatch(switchMode(newColorScheme))
    };

    useEffect(() => {
        handleColorSchemeChange(colorScheme);
    }, [colorScheme]);

  return (
      <Provider store={store}>
      {
          loading ?

          <PaperProvider>
              <ActivityIndicator animating={true} color={MD2Colors.red800} />
          </PaperProvider>
              :
              <PaperProvider>
                      <SafeAreaView style={styles.safeProv} >
                          < NavigatorBottom></NavigatorBottom>
                      </SafeAreaView>
                  </PaperProvider>
      }
          </Provider>

          );
};

const styles = StyleSheet.create({
    safeProv: {
        flex :1 ,
        marginTop: StatusBar.currentHeight
    }
});