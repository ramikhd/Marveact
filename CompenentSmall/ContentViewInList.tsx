import React, {Component, useState} from 'react';
import { StyleSheet, Text, View, Image, Animated } from 'react-native';
import * as url from "url";
import {ImageUrl} from "./ImageUrl";
import {useTheme} from "@react-navigation/native";

export type ContentProps = {
    title: string,
    cover_url: string,
    overview: string,
    release_date: string
};
export function ContentViewInList(contentProps: ContentProps) {
    const { colors } = useTheme();

    const fadeAnim = React.useRef(new Animated.Value(0)).current;

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: 1000,
                useNativeDriver: true,

            }
        ).start();
    }, []);


    return (
        <Animated.View style={[styles.container, { opacity: fadeAnim }]}>
            <View style={styles.coverContainer}>
                <ImageUrl url={contentProps.cover_url} styleImg={styles.cover}></ImageUrl>
            </View>
            <View style={styles.infoContainer}>
                <Text style={[styles.title,{color : colors.text}]} >{contentProps.title}</Text>
                <Text style={styles.overview} numberOfLines={4}>
                    {contentProps.overview }
                </Text>
                <Text style={styles.releaseDate}>{contentProps.release_date}</Text>
            </View>
        </Animated.View>
    );
}

const styles = StyleSheet.create({
    container: {

        flexDirection: 'row',
        shadowColor: "red",
        shadowOffset: {
            width: 10,
            height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 3.84,
        elevation: 5,
    },
    coverContainer: {
        width: 120,
        height: 200,
        borderRadius: 50,
        marginLeft: 10,
        backgroundColor: "#990000",
    },
    cover: {
        width: '100%',
        height: '100%',
        borderRadius: 100,
    },
    infoContainer: {
        marginHorizontal: 5,
        justifyContent: 'center',
    },
    title: {
        fontSize: 22,
        width:200,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    overview: {
        fontSize: 12,
        width:230,
        marginBottom: 5,
        color: 'grey',
    },
    releaseDate: {
        fontSize: 10,
        color: 'grey',
    },
});
