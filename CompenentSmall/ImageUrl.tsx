import React, {Component, useState} from 'react';
import { StyleSheet, Text, View, Image, Animated } from 'react-native';
import * as url from "url";

export type ImageUrlProps = {
    url: string,
    styleImg :any ,
    defaultImg? : string,
};

export function ImageUrl(imageUrlProps: ImageUrlProps) {

    const finalDefaultImage ='default.png'
    const finalDefaultImageSource= require(`../Image/${finalDefaultImage}`)
    const [defaultSource, setDefaultSource] = useState({uri : imageUrlProps.url});

    if(imageUrlProps.url != defaultSource.uri && defaultSource != finalDefaultImageSource  ){
        setDefaultSource( {uri : imageUrlProps.url})
    }

    const handleImageError = () => {
        setDefaultSource(finalDefaultImageSource);
    };

    return (
        <Image source={defaultSource} onError={handleImageError} style={imageUrlProps.styleImg} />
    );

}


