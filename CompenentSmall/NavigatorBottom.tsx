import {StyleSheet, Text, View, Image, Button, ScrollView, FlatList, useColorScheme} from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';


import React from 'react';
import StackNavigatorHome from "../Stacks/StackNavigatorHome";
import {NavigationContainer, useTheme} from "@react-navigation/native";
import SettingsMarveacte from "../Pages/SettingsMarveacte";
import { ColorfulTabBar } from 'react-navigation-tabbar-collection';

import StackNavigatorSeries from "../Stacks/StackNavigatorSeries";
import stackNavigatorFilms from "../Stacks/StackNavigatorFilms";
import StackNavigatorFavorites from "../Stacks/StackNavigatorFavorites";
import StackNavigatorMarveact from "../Stacks/StackNavigatorMarveact";
import {
    DefaultTheme,
    DarkTheme, } from '@react-navigation/native';
import {useSelector} from "react-redux";



export default function NavigatorBottom() {

    const { colors } = useTheme();
    const Tab = createBottomTabNavigator();
    const MyTheme = {
        ...DefaultTheme,
        colors: {
            ...DefaultTheme.colors,
            primary: 'rgb(255, 45, 85)',
        },
    };

    const scheme = useSelector(state => state.appReducer.mode)

    const color={
        primary: "#cc0000",
        secondary: "#6c757d",
        success: "#198754",
        danger: "#c9379d",
        warning: "#e6a919",
        info: "#00bcd4",
        light: scheme=='dark' ?colors.text: colors.background,       //Background Color
        dark: scheme=='dark' ?colors.text :colors.background,        //Foreground Color
    }
    return (
        <NavigationContainer  theme={scheme === 'dark' ? DarkTheme : DefaultTheme} options={{ headerShown: false }}>
            <Tab.Navigator
                tabBar={(props) => <ColorfulTabBar {...props } colorPalette={color} />}
            >
                <Tab.Screen name="Home" component={StackNavigatorHome}
                            options={{
                                title: 'Home',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/home.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />
                <Tab.Screen name="TV Show" component={StackNavigatorSeries}
                            options={{
                                title: 'Series',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/tvShow.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />
                <Tab.Screen name="Film" component={stackNavigatorFilms}
                            options={{
                                title: 'Film',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/film.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />

                <Tab.Screen name="Marveact" component={StackNavigatorMarveact}
                            options={{
                                title: 'Marveact',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/marveacte.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />
                <Tab.Screen name="Favorite" component={StackNavigatorFavorites}
                            options={{
                                title: 'Favorite',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/favorite.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />

                <Tab.Screen name="Settings" component={SettingsMarveacte}
                            options={{
                                title: 'Settings',
                                headerShown: false,
                                tabBarIcon: ({color}) => <Image
                                    source={require('../Image/settings.png')}
                                    style={{width: 25, height: 25}}
                                />,
                            }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
}

