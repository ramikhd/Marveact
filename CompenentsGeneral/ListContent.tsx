import React, { useState, useEffect } from 'react';
import {useNavigation, useTheme} from "@react-navigation/native";
import { Content } from "../Modele/Content";
import { Divider, Searchbar } from "react-native-paper";
import { StyleSheet, Text, View, Image, Button, ScrollView, FlatList, TouchableOpacity, TextInput, Animated } from 'react-native';
import { ContentViewInList } from '../CompenentSmall/ContentViewInList';

export type ListContentProps = {
    listContent: Array<Content>
    searchBar?: boolean
};

export default function ListContent(listContentProps: ListContentProps) {
    const navigation = useNavigation();
    const [searchQuery, setSearchQuery] = useState('');
    const [translateX, setTranslateX] = useState(new Animated.Value(-100)); // start position

    var shuffle = require('shuffle-array')
    var loading = [
        'https://cdn.discordapp.com/attachments/971095655823069225/1079058373728804915/loadAnimation.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExZmVkYjNiMDdhMTAwOTE4NTI0ZTU5OWJkY2VkNGNjMTNkODhkZTlhZSZjdD1z/dC3uDJazzlM4z3lZJ5/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOWJkNTA0Y2MwYzMwZTM4NjNmMjg2OTUxY2UxNzhiMmJhNWRhYTM0MSZjdD1z/fZ91xzFtKWmoJSD4TK/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOGVhODc3YjE0YzliZjRjODkxMTQ5Y2MxYzMxOTllMmJhMDUwNTE0MSZjdD1z/fiPtItaZc3wtJjOsjh/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExYmJmNzEzYzQ5NmQ0MmZmMzU3NWJkNzJkMjU0ZDlhOGRmNTI2YjA5NiZjdD1z/iVS4wrp1XT3BqUAQjo/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExYmJmNzEzYzQ5NmQ0MmZmMzU3NWJkNzJkMjU0ZDlhOGRmNTI2YjA5NiZjdD1z/iVS4wrp1XT3BqUAQjo/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExYmM0MjkzNDFmNzgwNzlhNGYyZjg5YzI3ODlmODUwOWQ1YmZlMzVjYyZjdD1z/Wq8vY2NXRelQhH9ms3/giphy.gif',
        'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOTYyMjVkOTBlZmVmNTc2ZGUyNzE5ZjI0OGRiYzg5MDY5MGNlOWMxNCZjdD1z/V5gQbaYZ5Lnvv1sSIr/giphy.gif',
        'https://media.giphy.com/media/U8XjIgP3Q0T9ZdtH69/giphy.gif'
    ];

    shuffle(loading);


    useEffect(() => {
        Animated.loop(
            Animated.timing(translateX, {
                toValue: 400,
                duration: 2000,
                useNativeDriver: true,
            })
        ).start();
    }, []);

    const filteredList = listContentProps.listContent.filter(
        item =>
            item.title.toLowerCase().includes(searchQuery.toLowerCase())
    );
    const { colors } = useTheme();
    return (
        <View style={{ display: "flex" ,alignContent: 'center'}}>
            {listContentProps.searchBar ?
                <>
                    <Searchbar
                        style={styles.searchInput}
                        placeholder="Search"
                        onChangeText={(text) => setSearchQuery(text)}
                        value={searchQuery}
                    />
                    <Divider />
                </>
                :
                <></>
            }

            {filteredList.length > 0 ? (
                <FlatList
                    fadingEdgeLength={25}
                    snapToInterval={240}
                    decelerationRate={"fast"}
                    data={filteredList}
                    renderItem={({ item }) => (
                        <View style={styles.item}>
                            <TouchableOpacity style={styles.view} onPress={() => navigation.navigate('Detail', { content: item })}>
                                <ContentViewInList title={item.title} release_date={item.release_date} cover_url={item.cover_url} overview={item.overview} />
                            </TouchableOpacity>
                        </View>
                    )}
                    keyExtractor={item => item.name}
                />
            ) : (
            <View>
                <Animated.Text
                    style={{
                        transform: [{ translateX: translateX }],
                        alignContent: 'center',
                        color : colors.text
                    }}>
                    No Content found
                </Animated.Text>
                <View style={{ flex: 1, alignContent:'center',alignItems:'center' }}>
                    <Image source={{ uri: loading[0] }} style={{ width: 250, height: 250 }} resizeMode={"contain"} />
                </View>
            </View>
            )}
        </View>


                );
};

const styles = StyleSheet.create({
    item: {
        marginVertical: 20,
    },
    searchInput: {
        height: 50,
        borderColor: 'transparent',
        marginTop:10,
        marginBottom:10,
        paddingHorizontal: 10,
    }
});
