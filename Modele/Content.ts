export class Content {
    private _id :number;
    private _title: string;
    private _release_date: string;
    private _overview: string;
    private _cover_url: string;
    private _trailer_url :string;
    private _directed_by: string;
    private _saga :string;
    private _imdb_id :string;

    constructor(id: number, title: string, release_date: string,overview: string,cover_url: string,trailer_url :string,directed_by: string,saga :string,imdb_id: string) {
        this._id = id;
        this._directed_by=directed_by ;
        this._saga= saga;
        this._imdb_id= imdb_id;
        this._title=title;
        this._release_date= release_date;
        this._overview= overview;
        this._cover_url= cover_url;
        this._trailer_url= trailer_url;

    }

    get imdb_id() {
        return this._imdb_id;
    }

    set imdb_id(value) {
        this._imdb_id = value;
    }
    get saga() {
        return this._saga;
    }

    set saga(value) {
        this._saga = value;
    }
    get directed_by() {
        return this._directed_by;
    }

    set directed_by(value) {
        this._directed_by = value;
    }
    get trailer_url() {
        return this._trailer_url;
    }

    set trailer_url(value) {
        this._trailer_url = value;
    }
    get cover_url() {
        return this._cover_url;
    }

    set cover_url(value) {
        this._cover_url = value;
    }
    get overview() {
        return this._overview;
    }

    set overview(value) {
        this._overview = value;
    }
    get release_date() {
        return this._release_date;
    }

    set release_date(value) {
        this._release_date = value;
    }
    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
}