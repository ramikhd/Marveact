import React, {useState, useRef, useEffect} from 'react';
import {
    View,
    Text,
    Image,
    ScrollView,
    Linking,
    StyleSheet, TouchableOpacity, ImageBackground,
} from 'react-native';
import {Content} from "../Modele/Content";
import {ImageUrl} from "../CompenentSmall/ImageUrl";
import {addFavorite, IsFavorites, removeFavorite, } from "../Redux/Actions/ActionContent";
import store from "../Redux/Store";
import { useSelector} from "react-redux";
import {IconButton, MD3Colors} from "react-native-paper";


import {useTheme} from "@react-navigation/native";

type ContentDetail = {
   content: Content
};




export function DetailContent( contentDetail: Any ) {
    const { colors } = useTheme();
    contentDetail=contentDetail.route.params.content
    var inFavorite =  useSelector(state => state.appReducer.favoriteContents).findIndex(
        (content) => content.title === contentDetail.title)

    return (

            <ScrollView fadingEdgeLength={25}  snapToInterval={2030} decelerationRate={'fast'} horizontal={false} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} >
              <ImageUrl styleImg={styles.coverImage} url={contentDetail.cover_url } />


                <ImageBackground
                    style={styles.backGroundImage}
                    resizeMode='cover'
                    source={{uri :contentDetail.cover_url }}
                    blurRadius={75}
                >
                <View  style={styles.detailView}>


                        <View style={styles.headContainer}>

                        <Text style={styles.title}>{contentDetail.title}</Text>

                            <IconButton

                                icon={inFavorite !=-1 ? "star-shooting" : "star-shooting-outline"  }
                                iconColor={MD3Colors.error50}
                                size={20}
                                onPress={() =>

                                    inFavorite !=-1 ?
                                    store.dispatch(removeFavorite(contentDetail))  :
                                      store.dispatch(addFavorite(contentDetail))
                            }
                            />

                        <TouchableOpacity onPress={() => Linking.openURL("https://www.imdb.com/title/"+contentDetail.imdb_id)} >
                            <Image
                                source={require('../Image/IMDb-Logo.wine.png')}
                                style={styles.imageIconSize}
                            />
                        </TouchableOpacity>


                        { contentDetail.trailer_url ?
                            <TouchableOpacity style={styles.trailerContainer}
                                              onPress={() => Linking.openURL(contentDetail.trailer_url)}
                            >
                                <Image
                                    source={require('../Image/trailer.png')}
                                    style={styles.trailer}
                                />
                            </TouchableOpacity>
                            :
                            <></>
                        }
                    </View>


                    <Text style={styles.releaseDate}>
                        Release Date: {contentDetail.release_date}
                    </Text>
                    <Text style={styles.overview}>
                        {contentDetail.overview ? contentDetail.overview : 'Coming Soon'}
                    </Text>

                    { contentDetail.directed_by ?
                        <Text style={styles.directedBy} >
                            Directed By: {contentDetail.directed_by}
                        </Text>
                        :
                        <></>
                    }
                    <Text style={styles.saga}>Saga: {contentDetail.saga}</Text>



                </View>
                </ImageBackground>
            </ScrollView>


    );
};

const styles = StyleSheet.create({
    coverImage: {
        width: 400,
        height: 700,

    },
    headContainer : {
        flexDirection: 'row',
        alignItems: 'center',
    },
    title: {
        fontSize: 30,

        width:200,
        fontWeight: 'bold',
        marginBottom: 5,
       /* fontSize: 30,
        fontWeight: 'bold',
        color: '#fff',
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 10,
        borderRadius: 10,*/
    },
    backGroundImage:{



    },
    detailView: {
        backgroundColor: 'rgba(255,255,255,0.4)',
        flex: 1,
        padding: 20,

    },
    releaseDate: {
        fontSize: 10,
        marginBottom: 10,
    },
    overview: {
        fontSize: 14,
        marginBottom: 10,
    },
    directed: {
        fontSize: 18,
        marginBottom: 10,
    },
    directedBy: {
        fontWeight: 'bold',
        fontSize: 18,
        marginBottom: 10,
    },
    saga: {
        fontSize: 18,
        marginBottom: 10,
    },
    imageIconSize: {
        width:25,
        height:25,
    },
    trailer: {
        padding:10,
        paddingLeft:10,
        backgroundColor: 'rgba(153, 0, 0,0.75)',
        borderRadius: 5,
        width:50,
        height:25,
    },
    trailerContainer: {
        padding:10,
        paddingLeft:10,

    },

});




