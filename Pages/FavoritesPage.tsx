import {StyleSheet, Text, View, Image, Button, ScrollView, FlatList, TouchableOpacity} from 'react-native';

import React from 'react';
import ListContent from "../CompenentsGeneral/ListContent";
import { useSelector} from "react-redux";


export default function FavoritesPage() {


    const contentsList = useSelector(state => state.appReducer.favoriteContents);

    return (
        <View>

                <ListContent listContent={contentsList}></ListContent>

        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        marginVertical: 10,
    },
});
