import {StyleSheet, Text, View, Image, Button, ScrollView, FlatList, TouchableOpacity} from 'react-native';

import React, { useState, useEffect } from 'react';
import ListContent from "../CompenentsGeneral/ListContent";
import { useSelector} from "react-redux";

import {StatusBar} from "expo-status-bar";


export default function FilmsPage() {


    const contentsList = useSelector(state => state.appReducer.films).sort((content1, content2) => new Date(content2.release_date) - new Date(content1.release_date));

    return (
        <View>
            {contentsList ? (
                <ListContent listContent={contentsList}></ListContent>
            ) : (
                <Text>Loading...</Text>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        marginVertical: 10,
    },
});
