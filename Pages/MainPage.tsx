import {StyleSheet, View} from 'react-native';

import React from 'react';
import ListContent from "../CompenentsGeneral/ListContent";
import { useSelector} from "react-redux";


export default function MainPage() {
    const contentsList =(useSelector(state => state.appReducer.films).concat(useSelector(state => state.appReducer.series))).sort(() => 0.5 - Math.random()).slice(0, 15);

    return (
        <View>
            <ListContent listContent={contentsList}></ListContent>
        </View>
    );
};

