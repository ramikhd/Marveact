import {  View} from 'react-native';

import React from 'react';
import ListContent from "../CompenentsGeneral/ListContent";
import { useSelector} from "react-redux";


export default function MarveactPage() {

    const contentsList =(useSelector(state => state.appReducer.films).concat(useSelector(state => state.appReducer.series))).sort((content1, content2) => new Date(content2.release_date) - new Date(content1.release_date));

    return (
        <View>

                <ListContent listContent={contentsList} searchBar={true}></ListContent>

        </View>
    );
};


