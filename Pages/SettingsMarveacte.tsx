import { StatusBar } from 'expo-status-bar';
import {StyleSheet,  View} from 'react-native';

import React, { useState, useEffect } from 'react';
import {useDispatch, useSelector} from "react-redux";
import {switchMode} from "../Redux/Actions/ActionContent";
import SwitchButton from "@freakycoder/react-native-switch-button";

export default function SettingsMarveacte() {
    const modeDef = useSelector(state => state.appReducer.mode);
    const dispatch = useDispatch();
    const [mode, setMode] = useState(modeDef);

    const handleThemeChange = () => {
        dispatch(switchMode(mode === 'light' ? 'dark' : 'light'));
        console.log(modeDef)
    }

    useEffect(() => {
       setMode(modeDef);
    }, [modeDef]);

    return (

        <View style={styles.container}>
            <SwitchButton
                inactiveImageSource={mode === 'light' ? require("../Image/moonDarkMood.png") : require("../Image/sunlightMood.png")}
                activeImageSource={mode === 'light' ? require("../Image/moonDarkMood.png") : require("../Image/sunlightMood.png")}
                mainColor="#cc0000"
                tintColor="#cc0000"
                style={styles.item}
                imageStyle={styles.image}
                text={mode === 'light' ? 'Dark mode' : 'Light mode'}

                textStyle={{
                    color: "#cc0000",
                    fontWeight: "600",
                }}
                onPress={handleThemeChange}
            />
{/*
            <StatusBar backgroundColor= {mode === 'light' ? 'white' : 'black'} style= {mode === 'light' ? 'dark' : 'light'}  />
*/}
        </View>

    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image:{
        width:50,
        height:50
    },

    item:{
        width:75,
        height:75
    }
});