![](Doc/MainS.png)   

# **Marveact**

## Bonjour et bienvenue sur le dépôt ! 👋

*******
 
<div align="center">

### Sommaire <br/>

 [Introduction](#introduction) | [Equipe](#team) | [Fonctionalitées](#feature)|[Capture d'écran](#screen)
 
</div>

<div id='introduction'/>

*******

## **Introduction au projet** :bulb:

Dans le cadre de mon formation, nous avons eu l'opportunité de développer un projet qui nous a permis de mettre en pratique les compétences acquises au fil des enseignements. Notre objectif principal était de mettre en place une application qui est développer avec reactnative et qui répond à un certin barehme. 

**Un peu de vocabulaire: 
React Native est un framework d'applications mobiles open source créé par Facebook. Il est utilisé pour développer des applications pour Android, iOS et UWP en permettant aux développeurs d'utiliser React avec les fonctionnalités natives de ces plateformes.**

*******
<div id='team'/>

## **Présentation de l'équipe** :busts_in_silhouette:

Étudiant de deuxième année - BUT Informatique - IUT Clermont Auvergne - 2022-2023   
`KHEDAIR Rami`

*******  

<div id='feature'/>

## **Fonctionalitées** :computer:

- Contexte

Mon application marveact, qui est développée en R**EACT** Native et est basée sur l'univers de **MARV**el. L'application consomme une API qui renvoie des informations sur les séries et les films de Marvel.

- Application

Les fonctionnalités prévues de l'application sont la visualisation et la consultation des films et des séries, l'affichage de pages de détails, la possibilité de changer de mode d'affichage entre le mode sombre et le mode clair, la recherche de films et de séries, la gestion des favoris / déjà vu et la gestion du compte utilisateur.

- Fonctionnalités non réalisées

Les fonctionnalités non réalisées sont la gestion du compte et la fonction "déjà vu", car elles n'apportent pas de valeur ajoutée à l'application ou à mon apprentissage.

<div id='screen'/>

## **Screenshot** :camera_flash:

- Quand vous arriver à l'application vous avez un list aléatoir des films et series de marvel :<br/>
<br/><div align="center">
    <img  height="450"   src="Doc/Home.jpg"/> 
</div><br/>

- Il y a deux lists principal, Films 🎥 et Series 📺:<br/>
<br/><div align="center">
    <img  height="450"   src="Doc/Film.jpg"/> 
    <img  height="450"   src="Doc/Series.jpg"/>
</div><br/>

- Vous pouver checher dans ces lists en utilisant le bar de recherche 🔍 dans le page Marveact:<br/>
<br/><div align="center">
    <img  height="450" src="Doc/search.jpg"/> 
</div><br/>

- Un systeme de Favoris 🌟 est mise en place:<br/>
<br/><div align="center">
    <img  height="450" src="Doc/favori.jpg"/> 
</div><br/>

- Vous pouver aussi changer le mode couleur entre dark 🌙 et light 💡 mode:<br/>
<br/>
<div align="center">
<img height="450" src="Doc/lightMode.jpg"/>
<img height="450" src="Doc/DarkMode.jpg"/>
</div><br/>
<div align="center">
<img height="450" src="Doc/search.jpg"/>
<img height="450" src="Doc/searchDarkMode.jpg"/>
</div>
<br/>

 *également l'application detecte si vous changer le mode de votre téléphone  donc vous avez deux choix soit depuis l'application 📱, soit depuis les paramètres du téléphone.⚙️ *<br/>

- Bien sûr il y a la possibilité de clicker sur le film ou la série et vous aller avoir plus de information 📰 et le possibilité de l'ajouter ➕ dans votre favoris 🤩 si vous clicker sur l'étoile, même aller voir le bande-annonce ou plus d'information sur IMDB<br/>

<br/><div align="center">
<img  height="450" src="Doc/DetailFirstPage.jpg"/>
</div>

<div align="center">

### *on swipe down* 

</div>

<div align="center">
<img  height="450" src="Doc/DetailSecond.jpg"/> 
</div><br/>


</div>

*******

<div align="center">

### 👋 Fin 👋

</div><br/>

*******

