import {
    ADD_FAVORITE, FAVORITELIST,
    FETCH_FAVORITES,
    FETCH_FILM,
    FETCH_SERIES,
    IS_FAVORITE,
    REMOVE_FAVORITE,
    SAVE_FAVORITE, THEME_CHANGE
} from '../Constants';
import {Content} from "../../Modele/Content";
import AsyncStorage from "@react-native-async-storage/async-storage";
import ListContent from "../../CompenentsGeneral/ListContent";
import {ColorSchemeName, useColorScheme} from "react-native";

export const setFilmList = (filmList: Content[]) => {
    return {
        type: FETCH_FILM,
        payload: filmList,
    };
}

export const getFilmList = () => {
    return async dispatch => {
        try {
            const filmsPromise = await fetch('https://mcuapi.herokuapp.com/api/v1/movies?page=1&order=chronology%2CDESC');
            const filmsListJson = await filmsPromise.json();
            const filmsList: Content[] = filmsListJson.data.filter(elt => new Content(elt["id"],elt["title"],elt["release_date"] ,elt["overview"],elt["cover_url"],elt["trailer_url"] ,elt["directed_by"],elt["saga"] ,elt["imdb_id"]) ).sort((content1, content2) => new Date(content2.release_date) - new Date(content1.release_date));
            dispatch(setFilmList(filmsList));

        } catch (error) {
            console.log('Error---------', error);

        }
    }
}

export const setSeriesList = (seriesList: Content[]) => {
    return {
        type: FETCH_SERIES,
        payload: seriesList,
    };
}

export const getSeriesList = () => {
    return async dispatch => {
        try {
            const seriesPromise = await fetch('https://mcuapi.herokuapp.com/api/v1/tvshows');
            const seriesListJson = await seriesPromise.json();
            const seriesList: Content[] = seriesListJson.data.filter(elt => new Content(elt["id"],elt["title"],elt["release_date"] ,elt["overview"],elt["cover_url"],elt["trailer_url"] ,elt["directed_by"],elt["saga"] ,elt["imdb_id"]) );
            dispatch(setSeriesList(seriesList));

        } catch (error) {
            console.log('Error---------', error);

        }
    }


}

export const setFavoritesList = (favoritesList: Content[]) => {
    return {
        type: FETCH_FAVORITES,
        payload: favoritesList,
    };
}



export const getFavoritesList = () => {
    return async dispatch => {
        try {
            const jsonContents = await AsyncStorage.getItem(FAVORITELIST)
            console.log("Sauvegarde est recuperée", jsonContents);
            dispatch(setFavoritesList(jsonContents != null ? JSON.parse(jsonContents) :  [] ));
            return jsonContents != null ? JSON.parse(jsonContents) : [];
        } catch (error) {
            console.log('Error loading lists from local storage:', error);
        }
    }
}

export const addFavorite = (content: Content) => ({
    type: ADD_FAVORITE,
    payload: content,
});

export const removeFavorite = (contentId: number) => ({
    type: REMOVE_FAVORITE,
    payload: contentId,
});

export  const  saveListToLocalStorage=async (listContent: Content[],name :string) => {
    try {
        const jsonContentList = JSON.stringify(listContent)
        await AsyncStorage.setItem(name, jsonContentList);
        console.log("Sauvguarde est fait");
        console.log(listContent);
    } catch (e) {
        console.log("An error occurred", e);
    }
}

export const IsFavorites = (content: Content) => ({
    type: IS_FAVORITE,
    payload: content,
});

export const switchMode = (mode: ColorSchemeName) => {
    return {
        type: THEME_CHANGE,
        payload: mode,
    };
};