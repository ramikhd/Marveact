export const FETCH_FILM = 'FETCH_FILM';
export const FETCH_SERIES = 'FETCH_SERIES';
export const FETCH_FAVORITES = 'FETCH_FAVORITES';
export const ADD_FAVORITE = 'ADD_FAVORITE';
export const REMOVE_FAVORITE = 'REMOVE_FAVORITE';
export const SAVE_FAVORITE = 'SAVE_FAVORITE';
export const IS_FAVORITE = 'IS_FAVORITE';
export const FAVORITELIST = 'FavoritsList3';
export const THEME_CHANGE = 'THEME_CHANGE';


