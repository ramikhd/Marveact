import {
    ADD_FAVORITE, FAVORITELIST, FavoritsList,
    FETCH_FAVORITES,
    FETCH_FILM,
    FETCH_SERIES, IS_FAVORITE,
    REMOVE_FAVORITE, SAVE_FAVORITE, THEME_CHANGE
} from "../Constants";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {saveFavoritesToLocalStorage, saveListToLocalStorage} from "../Actions/ActionContent";
import * as Console from "console";
import {StatusBar, useColorScheme} from "react-native";
const initialState = {
    contents: [],
    films: [],
    series: [],
    favoriteContents: [],
    watchedContents: [],
    mode: "",
}


export default appReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_FILM:
            return {...state, films: action.payload};

        case FETCH_SERIES:
            return {...state, series: action.payload};

        case FETCH_FAVORITES:
            console.log("hi", action.payload)
            return {...state, favoriteContents: action.payload};

        case ADD_FAVORITE:
            const index = state.favoriteContents.findIndex(
                (content) => content.title === action.payload.title
            );
            console.log(state)
            if (index === -1) {
                saveListToLocalStorage([...state.favoriteContents, action.payload], FAVORITELIST)
                return {
                    ...state,
                    favoriteContents: [...state.favoriteContents, action.payload]
                };
            } else {

                saveListToLocalStorage([...state.favoriteContents], FAVORITELIST)
                return state;
            }
        case REMOVE_FAVORITE:
            saveListToLocalStorage([...state.favoriteContents.filter(item => item !== action.payload)], FAVORITELIST)
            return {...state, favoriteContents: state.favoriteContents.filter(item => item !== action.payload)};

        case SAVE_FAVORITE:
            return saveListToLocalStorage(state.favoriteContents, FAVORITELIST);
        case IS_FAVORITE:
            return true
        case THEME_CHANGE:
            StatusBar.setBackgroundColor(action.payload == 'light' ? 'white' : 'black');
            StatusBar.setBarStyle(action.payload == 'dark' ? 'light-content' : 'dark-content')
            console.log("Action Mode : ", (action.payload))

            return {
                ...state,
                mode: action.payload
            }
        default:
            return state;
    }


}

// Handle our action of changing the theme
