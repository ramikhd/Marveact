import {configureStore} from '@reduxjs/toolkit'
import appReducer from './Reducers/Reducers';

const reducer = {
    appReducer: appReducer,
}

const store = configureStore({
        reducer,
    },
);

export default store;