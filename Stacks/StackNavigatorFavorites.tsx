import {StyleSheet, Text, View, Image, Button, ScrollView, FlatList, Settings} from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {NavigationContainer} from "@react-navigation/native";

import 'react-native-gesture-handler';

import React, { useState, useEffect } from 'react';
import {DetailContent} from "../Pages/DetailContent";
import SeriesPage from "../Pages/SeriesPage";
import FilmsPage from "../Pages/FilmsPage";
import FavoritesPage from "../Pages/FavoritesPage";


export default function StackNavigatorFavorites() {
    const Stack = createStackNavigator();

    return (

            <Stack.Navigator initialRouteName="FavoritesPage">
                <Stack.Screen  name="Favorites Page" component={FavoritesPage} options={{headerShown:false}}/>
                <Stack.Screen name="Detail" component={DetailContent} options={{headerShown:false}}/>
            </Stack.Navigator>

    )
};

const styles = StyleSheet.create({

    item: {
        marginVertical: 10,
    },
});
