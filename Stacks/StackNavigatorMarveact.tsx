import {StyleSheet} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import React from 'react';
import {DetailContent} from "../Pages/DetailContent";
import MarveactPage from "../Pages/MarveactPage";


export default function StackNavigatorMarveact() {
    const Stack = createStackNavigator();

    return (

            <Stack.Navigator initialRouteName="FilmsPage">
                <Stack.Screen  name="Films Page" component={MarveactPage} options={{headerShown:false}}/>
                <Stack.Screen name="Detail" component={DetailContent} options={{headerShown:false}}/>
            </Stack.Navigator>

    )
};

const styles = StyleSheet.create({

    item: {
        marginVertical: 10,
    },
});
