import {StyleSheet, Text, View, Image, Button, ScrollView, FlatList, Settings} from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {NavigationContainer} from "@react-navigation/native";

import 'react-native-gesture-handler';

import React, { useState, useEffect } from 'react';
import {DetailContent} from "../Pages/DetailContent";
import SeriesPage from "../Pages/SeriesPage";


export default function StackNavigatorSeries() {
    const Stack = createStackNavigator();

    return (

            <Stack.Navigator initialRouteName="SeriesPage">
                <Stack.Screen  name="Series Page" component={SeriesPage} options={{headerShown:false}}/>
                <Stack.Screen name="Detail" component={DetailContent} options={{headerShown:false}}/>
            </Stack.Navigator>

    )
};

const styles = StyleSheet.create({

    item: {
        marginVertical: 10,
    },
});
